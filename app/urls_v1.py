from django.urls import path

from rest_framework import routers

from .views import (
    sign_in, sign_up,
    send_email,

    global_search,

    UserViewSet,

    TaskListView,
    TaskDetailView,
)

app_name = 'app'

urlpatterns = [
    path('auth/signin/', sign_in),
    path('auth/signup/', sign_up),
    path('sendmail/', send_email),
    path('tasks/', TaskListView.as_view()),
    path('tasks/<int:pk>/', TaskDetailView.as_view()),
    path('search/', global_search),

]
router = routers.SimpleRouter()

router.register(r"users", UserViewSet, basename='users')

urlpatterns += router.urls