from rest_framework import serializers

from .models import User
from .models import Task


class UserSerializer(serializers.ModelSerializer):
    date_joined = serializers.DateTimeField(format="%Y.%m.%d %H:%M:%S")

    class Meta:
        model = User
        fields = [
            "id",
            "email",
            "first_name",
            "second_name",
            "patronymic",
            "date_joined",
            "avatar",
            "position",
            "sex",
            "departament",
            "privacy_policy"
        ]


class TaskSerializer(serializers.ModelSerializer):
    date_format = "%Y-%m-%d"

    start_date = serializers.DateField(format=date_format)
    end_date = serializers.DateField(format=date_format)

    class Meta:
        model = Task
        fields = ["id", "number", "text", "start_date", "end_date", "status", "director_id", "responsible_id"]
