import transliterate

from django.core.exceptions import ValidationError
from django.core.validators import EmailValidator

from django.db.models import Q

from django.core.mail import EmailMessage
from django.http import JsonResponse

from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser
from rest_framework.viewsets import ViewSet

from .custom_backend import CustomBackend

from .models import User
from .models import Task

from .serializers import UserSerializer
from .serializers import TaskSerializer


@api_view(["POST"])
def sign_in(request):
    email = request.data.get("email")
    password = request.data.get("password")

    if not (email and password):
        return Response(
            data={"detail": "Need dictionary {email:<email>, password:<password>}"},
            status=400,
        )
    custom_backend = CustomBackend()
    user = custom_backend.authenticate(email=email, password=password)
    if not user:
        return Response(data={"detail": "Email or password incorrect"}, status=401)

    return Response(data={"authorization_token": user.auth_token.key, "id": user.id}, status=200)


@api_view(["POST"])
def sign_up(request):
    """
           Create user.

           Request and response examples:
          ```
          Response value:
            {
                "email": "admin@mail.ru",
                "password": "admin",
                "first_name": "Антуан",
                "second_name": "Хуан",
                "patronymic": "Олегович"
            }

    """

    first_name = request.data.get("first_name")
    second_name = request.data.get("second_name")
    password = request.data.get("password")
    patronymic = request.data.get("patronymic")
    email = request.data.get("email")

    if not (first_name and second_name and password and email and patronymic):
        return Response(
            data={
                "detail": "Need dictionary {first_name:<first_name>, second_name:<second_name>, "
                          "patronymic: <patronymic>, email:<email>, password:<password>} "
            },
            status=406,
        )

    if User.objects.filter(email=email).exists():
        return Response(data={"detail": "This user already exists"}, status=400)

    email_validator = EmailValidator()
    try:
        email_validator(email)
    except ValidationError:
        return Response(
            data={"detail": "{} has validation error".format(email)}, status=406
        )
    new_user = User.objects.create_user(
        email=email,
        password=password,
        first_name=first_name,
        second_name=second_name,
        patronymic=patronymic
    )
    return Response(data={"authorization_token": new_user.auth_token.key, "id": new_user.id}, status=201)


class UserViewSet(ViewSet):
    #authentication_classes = [TokenAuthentication]
    #permission_classes = [IsAuthenticated]

    def list(self, request):
        queryset = User.objects.filter(is_active=True)
        serializer = UserSerializer(queryset, many=True)
        return Response(data=serializer.data)

    def update(self, request, pk):
        try:
            user = User.objects.get(pk=pk)
        except User.DoesNotExist:
            return Response(data={"detail": "User not found"}, status=404)

        data = request.data.get("user")
        serializer = UserSerializer(
            instance=user, data=data, partial=True
        )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        return Response(serializer.data, status=201)

    def retrieve(self, request, pk):
        try:
            queryset = User.objects.get(pk=pk)
        except User.DoesNotExist:
            return Response(data={"detail": "User not found"}, status=404)
        serializer = UserSerializer(queryset)
        return Response(serializer.data)

    def destroy(self, request, pk):
        try:
            queryset = User.objects.get(pk=pk)
        except User.DoesNotExist:
            return Response(status=404)
        queryset.delete()

        return Response(status=204)

    @action(detail=True, methods=['patch'], parser_classes=[MultiPartParser])
    def set_avatar(self, request, pk):
        """
        Set avatar. You need to download the avatar file and specify it in the headers.

        Response value:
        ```
        Example header string:
        Content-Type multipart/form-data
        Authorization Token 51bfef57318703720118e30ca80fb855fa12c9fd
        Content-Disposition attachment; filename=avatar.jpg
        ```
        """
        avatar = request.FILES.get('avatar')

        if not avatar:
            return Response(
                data={'detail': 'Image file required!'},
                status=400
            )
        try:
            avatar._name = '_'.join(transliterate.translit(avatar._name, reversed=True).split()).replace("'", '')
        except:
            pass
        if avatar.name.split('.')[-1] not in {'jpg', 'png'}:
            return Response(
                data={'detail': 'Image must be jpg or png!'},
                status=400
            )

        user = User.objects.get(pk=pk)
        user.avatar = avatar
        user.save()
        return Response(
            status=201,
            data={
                "avatar": user.avatar.url
            }
        )


class TaskListView(ListCreateAPIView):
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]
    queryset = Task.objects.all()
    serializer_class = TaskSerializer


class TaskDetailView(RetrieveUpdateDestroyAPIView):
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]
    queryset = Task.objects.all()
    serializer_class = TaskSerializer


@api_view(["POST"])
def send_email(request):
    """
           Send some mail.

           Request and response examples:
          ```
          Response value:
            {
                "message_text": "Some text",
                "message_subject": "Some subject",
                "mail_from": "Email from letter was sent",
                "mail_addresses": "List of addresses"
            }

    """
    message_text = request.data.get('message_text')
    message_subject = request.data.get('message_subject')
    mail_from = request.data.get('mail_from')
    mail_addresses = request.data.get('mail_addresses')

    email = EmailMessage(
        f'{message_subject}',
        f'{message_text}',
        f'{mail_from}',
        [mail_addresses]
    )
    email.send()


@api_view(["GET"])
def global_search(request):
    """
           Searching for all models in database.

           Example of your request: .../search/?query=<your_query>



          ```
          User fields: email, city, first_name, second_name, patronymic, position, sex, departament.
          Task fields: text, start_date, end_date, status.

    """

    query = request.GET.get("query", None)
    users = User.objects.all()
    tasks = Task.objects.all()

    if query:
        filter_users = users.filter(Q(email__icontains=query) | Q(city__icontains=query) |
                                    Q(first_name__icontains=query) | Q(second_name__icontains=query) |
                                    Q(patronymic__icontains=query) | Q(position__icontains=query) |
                                    Q(sex__icontains=query) | Q(departament__icontains=query))

        filter_tasks = tasks.filter(Q(text__icontains=query) | Q(start_date__icontains=query) |
                                    Q(end_date__icontains=query) | Q(status__icontains=query))

        return JsonResponse({"users": UserSerializer(filter_users, many=True).data,
                            "tasks": TaskSerializer(filter_tasks, many=True).data})
