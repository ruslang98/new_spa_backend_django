from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.base_user import BaseUserManager

from rest_framework.authtoken.models import Token


def get_avatar_path(instance, filename):
    return "avatars/{}/{}".format(instance.email, filename)


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        Token.objects.create(user=user)
        return user

    def create_user(self, email, password=None, **extra_fields):

        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    ZERO = 'ZO'
    LOGISTICS = 'LS'
    TECHNICAL = 'TL'
    DEVELOPMENT = 'DT'

    MAN = 'M'
    WOMAN = 'W'

    DEPARTAMENT_CHOICES = [
        (ZERO, 'Zero'),
        (LOGISTICS, 'Logistics'),
        (TECHNICAL, 'Technical'),
        (DEVELOPMENT, 'Development'),
    ]

    SEX_CHOICES = [
        (MAN, 'Man'),
        (WOMAN, 'Woman')
    ]

    email = models.EmailField(unique=True)
    city = models.CharField(max_length=30, null=True)
    first_name = models.CharField(max_length=30, null=True)
    second_name = models.CharField(max_length=30, null=True)
    patronymic = models.CharField(max_length=30, null=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_confirm = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=True)
    avatar = models.ImageField(null=True, upload_to=get_avatar_path)
    position = models.CharField(max_length=30, null=True)
    sex = models.CharField(max_length=30, choices=SEX_CHOICES, null=True)
    departament = models.CharField(max_length=30, choices=DEPARTAMENT_CHOICES, null=True)
    privacy_policy = models.BooleanField(default=False)
    objects = UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"


class Task(models.Model):
    COMPLETED = "C"
    PROCESS = "P"
    NEW = "N"

    STATUS = [(COMPLETED, "Завершено"),
              (PROCESS, "В процессе"),
              (NEW, "Новая задача")
              ]
    number = models.IntegerField(null=True)
    text = models.CharField(max_length=128)
    start_date = models.DateField()
    end_date = models.DateField()
    status = models.CharField(max_length=1, choices=STATUS, default=NEW)
    director_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name="director_name")
    responsible_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_name")

    class Meta:
        verbose_name = "Текущая задача"
        verbose_name_plural = "Текущие задачи"

    def __str__(self):
        return self.text
